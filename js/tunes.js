
function symlinkTune() { // open single link dialog
	if(window.uid != 'a') return;
	if(window.curUI != 'unsorted' && window.curUI.substr(0, 3) != 'alb') {
	        	popMessage(window.locale.errors.linkError);
	        	return;
        	}
        $( "#linktrk-form" ).dialog( "open" );
}
function symlinkImport() { // open album link dialog
	if(window.uid != 'a') return;
	    $( "#linkalb-form" ).dialog( "open" );
}
function keepTune() { // download btn handler
	downTune(window.curUrl);
}
function downTune(t) { // download current tune
	if(t.substring(0, 2)!= 'SC') {
		
		popMessage(window.locale.errors.DLStart);
		
		$('#f')[0].src='down.php?file='+encodeURIComponent(t);
	}
	else {
		popMessage(window.locale.errors.DLStartSC);
		window.location.href='sc_auth.php?act=stream_down&tid='+(t.substr(2, t.length));
		
	}
}
 function performZipDecomp(par) { // call to open zip decompress form
	 $('#zipname')[0].value=par.split(':')[1];
	         $('#album')[0].value='';
	          $('#artist')[0].value='';
	           $('#isCompilation')[0].value='';
	 	 $( "#dialog-form" ).dialog( "open" );
 }
 
 function shareTune() { // share a track link
 	if(window.uid == 'g') return;
 	if(window.curUrl.substr(0, 2) == 'SC') {
	 	//popMessage(window.locale.ui.linkMake);
	 	NProgress.start();
	 	SC.get("/tracks/"+window.curUrl.substr(2, window.curUrl.length), {limit:1}, function(data) { 
	 		NProgress.done();
				prompt(window.locale.dialogs.copyLink,data.permalink_url);
	 	 } );
			
		return;
 	}
	var link=window.location.href.split('#')[0].split('?')[0];
	var params= '';
	var trackNum = 0;
	if(window.isShufflin) {
		trackNum = (window.preShuffleQ.indexOf(window.curUrl)+1);
	} else {
		trackNum = (window.queue.indexOf(window.curUrl)+1);
	}
	if(window.playingFromUI.substring(0, 3) === 'alb') {
		params='?kind=album&album='+window.playingFromUI.substring(3, window.playingFromUI.length)+"&track="+trackNum.toString();
	} else 
	switch(window.playingFromUI) {
		case "music":
			params='?kind=album&album='+window.curAlbumID.toString()+"&track="+trackNum.toString();
			break;
		case "compilation":
			params='?kind=album&album='+window.curAlbumID.toString()+"&track="+trackNum.toString();
			break;
		case "unsorted":
			params="?kind=user&user="+window.uid.toString()+"&track="+trackNum.toString();
			break;
			
		default:
			params="?list="+window.playingFromUI+"&track="+trackNum.toString();;
	}
	prompt(window.locale.dialogs.copyLinkTrk,link+params);
}
