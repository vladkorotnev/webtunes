function renameList() { // rename a playlist
	var list=$('.lib.current')[0].id;
	if(list == 'music' || list == 'unsorted' || list == 'compilation'|| list.substr(0, 2) == 'sc') {
		return
	}
	var newN = prompt(window.locale.dialogs.plsRename,  $('.lib.current')[0].children[0].innerHTML);
	if(newN.length > 16) {
		newN = newN.substring(0, 15) + '…';
	}
	$.ajax({type:"POST", url:'renameList.php', data:{ "list":list, "name":newN } , success:function(data) {
				var inf= jQuery.parseJSON(data);
				$('#'+inf['id'])[0].children[0].innerHTML=inf['name'];
				
			}});
}

function shareList() { // share a playlist
	var list=$('.lib.current')[0].id;
	var link=window.location.href.split('#')[0].split('?')[0];
	if(list == 'music' || list == 'unsorted' || list == 'compilation' || list.substr(0, 3) == 'scs') {
		return
	}
	if(list.substr(0, 3) == 'scl') {
		popMessage(window.locale.ui.linkMake);
		$.ajax({type:"GET", url:'sc_auth.php', data:{ "act":"mklink", "id":list.substr(4, list.length), "kind":"list" } , success:function(data) {
				prompt(window.locale.dialogs.copyLink,data);
				
			}});
		return;
	}
	if(list.substr(0, 3) == 'alb') {
		link = link + '?kind=album&album='+list.substr(3, list.length);
	} else {
		link = link + '?list='+list.toString();
	}
	
	prompt(window.locale.dialogs.copyLink,link);
}


var tdellist;
function trashList() { // trash a playlist
	if(window.curUI == 'music' || window.curUI == 'unsorted' || window.curUI == 'compilation' || window.curUI.substr(0, 3) == 'alb' || window.curUI.substr(0, 3) == 'sc_' || window.curUI.substr(0, 3) == 'scs') {
		alert(window.locale.dialogs.trash);
		return;
	}
	var list=$('.lib.current')[0].id;
	tdellist=list;
	for (var i=0; i< $('.lib.current')[0].children.length; i++) {
		if($('.lib.current')[0].children[i].className == 'savebutton') {return;}
	}
	if (confirm(window.locale.dialogs.del1+$('.lib.current')[0].children[0].innerHTML+window.locale.dialogs.del2)) {
		writeUI(window.locale.ui.wait, window.curUI)
		if(list.substr(0, 4) != 'scl_') {
			$.ajax({type:"POST", url:'killlist.php',  data:{"list":list} , success:function(data) {
				$('.lib.current')[0].innerHTML='';
				writeUI('', window.curUI)
				popMessage(window.locale.errors.deleted);
					
			}});
		} else {
			$.ajax({type:"GET", url:'sc_auth.php', data:{"act":"killlist", "lid": list.substr(4, list.length)} ,   success:function(data) {
							if(data == '200 - OK') {
									$('.lib.current')[0].innerHTML='';
										writeUI(window.locale.ui.wait, window.curUI)
									popMessage(window.locale.errors.deleted);
							}		else {
								popMessage(window.locale.errors.fail);
								console.log(data);
							}			
						}});
	
		}
	}
}

function copyList(list) {
	 // Duplicate playlist of other user to our acct
	 $.ajax({type:"POST", url:'copyList.php', data:{"list":list} , success:function(data) {
	 			window.location.href = "./player.php"; //dirty fix
	 			$('#save'+list)[0].style.display="none";
	 			var ls = $('#'+list)[0];
	 			ls.id = data;
	 			var n = ls.children[1].innerHTML;
	 			
	 			ls.innerHTML='<a href="#" onclick="switchTo(\''+data+'\')">'+n+'</a>';
	 			switchTo(data);
			}});
 }
 
 var tListN=undefined;
function makeList(evt) {
	// new playlist
	if(evt.altKey) {
	 	newSCList();
		return;
	}
	 mkLst();
	 
 }
 
 function mkLst() {
	 tListN = prompt(window.locale.dialogs.newList,window.locale.dialogs.unamedList);
	 if(tListN == undefined) return;
	 
	 $.ajax({type:"POST", url:'mklist.php', data:{"name":tListN} , success:function(data) {
	 			var lawl='<li id="'+data+'" class="lib" ondragover="allowDrop(event)" ondragend="noDrop(event)" ondragleave="noDrop(event)" ondrop="drop(event)"><a href="#" onclick="switchTo(\''+data+'\')">'+tListN+'</a></li>';
	 			$('#playlists')[0].innerHTML = $('#playlists')[0].innerHTML + lawl;
	 			switchTo(data);
			}});
 }
 
