function loadVK(url, manually) {
		console.log("Aw shit its VK! Fall back to audio for the time being");
		window.audio = new Audio();
		var tid=url.substr(2, url.length);
		
		VK.api("audio.getById", {audios:tid}, function(data) {
      	  if(data.error) {
	      	  popMessage(data.error.error_msg);
      	  }
			window.audio.src = data.response[0].url; 
			window.audio.play();
			window.audio.addEventListener("progress", function(a)
				  {
					  try {
						   document.getElementById('progressbar-footer').style.width=(window.audio.buffered.end(0) / window.audio.duration)*100+"%";
						  if((window.audio.buffered.end(0) / window.audio.duration)*100 >= 100) {
							 document.getElementById('footer-process').style.display="none"; 
						  } else {
							  document.getElementById('footer-process').style.display="block";
						  }
						  
						  document.getElementById('process-sig').innerHTML=window.locale.ui.buffering;
					  } catch(e) {}
				  }
				);
			window.audio.addEventListener("stalled", function()
				  {
				  popMessage(window.locale.ui.bufferingStalled);
				  }
				);
			window.audio.addEventListener("canPlay", function()
				  {
					  hadstarted=true;
					document.getElementById('btn-pause').style.display="block";
					document.getElementById('btn-play').style.display="none";
					window.audio.volume = $( ".volume-bar" ).volslider('option','value') / 100;
					window.audio.play();
				  }
				);
				window.audio.addEventListener("ended", function(e)
				  {
				  	next();
				  }
				);
				window.audio.addEventListener("timeupdate", function(e)
				  {
				  	updateProg();
				  }
				);
				hadstarted=true;
				document.getElementById('btn-pause').style.display="block";
				document.getElementById('btn-play').style.display="none";
				window.audio.volume = $( ".volume-bar" ).volslider('option','value') / 100;
				$('#displ')[0].style.opacity="1";
				var title = data.response[0].title;
				var author = data.response[0].artist;
				
					document.getElementById('song-title').innerHTML=title;
					document.title = window.locale.appname+': '+title+' - '+author;
				
					console.log('Fallback, enabling seek');
				$( ".track-progress" ).seekslider('option','disabled',false);
				$( ".volume-bar" ).volslider('option','disabled', false);
				disableProg();
				document.getElementById('song-album').innerHTML="VK.com";
				document.getElementById('song-band').innerHTML=data.response[0].artist; 
				document.getElementById('sharer').style.display="none";
				document.getElementById('keeper').style.display="none";
				postMetadata(true, title, author, 'VK.com');
				clearTimeout(displayBandTimeout);
				showBand();
				clearTimeout(displayBandTimeout);
				for(var a=0; a < $('.now').length; a++) { $('.now')[a].style.display='none'; }
				for(var a=0; a < $('.format').length; a++) { $('.format')[a].style.display='inline-block'; }
				try {
					document.getElementById(url+'_format').style.display="none";
					document.getElementById(url+'_ico').style.display="inline-block";
				} catch(e) {}

        });
}

function searchVK () {
	// SoundCloud search handler
	$('#listing').empty();
	var q = $('#vksearchfield').val();
	var internalQ = q.replace(' ', '').replace('\'', '').replace('[','').replace(']','').replace('#','').replace('&','');
	var sd;
	if($('#vks_'+internalQ).length <= 0) {
		sd = $('#vk_search').clone(true);
		sd[0].style.display = "";
		sd[0].id = "vks_"+internalQ;
		var urq = q;
		if(urq.length > 16) {
			urq = q.substring(0, 15) + '…';
		}
		sd[0].innerHTML='<img src="img/search.png" class="search-icon"><a href="#" onclick="switchTo(\'vks_'+internalQ+'\')">'+urq+'</a>';
		$('#vks').append(sd);
	} else {
		sd = $('#vks_'+internalQ);
	}
	
	
	switchTo(sd[0].id);
	NProgress.start();
	$.ajax({type:"GET", url:'vk_auth.php', contentType:"application/x-www-form-urlencoded", data:{"q":q} , success:function(data) {
				writeUI(data, sd[0].id);
					NProgress.done();
			}});

	$('#vksearchfield').val('');
}
