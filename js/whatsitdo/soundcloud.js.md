# soundcloud.js
## SoundCloud integration routines

* `loadCloud(url)` is the routine to load a sound from SoundCloud by it's identifier (like `SC102935107`) and start its playback, for now falling back to the HTML5 `<audio>` tag.
* `moarSC(curs)` loads more of the feed by using a given pagination cursor
* `searchCloud()` handles the SoundCloud search queries