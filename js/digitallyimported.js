window.DITier = 'public3';
window.DIStationNames = new Object();
window.DIStationIDs = new Object();

function DI_LoadStations(uiname) {
	/*
	 ~ Tiers ~
	 public1	64kbps aac
	 public2	40kbps aac
	 public3	96kbps mp3
	 premium	128kbps aac
	 premium_high	256kbps mp3
	 HTML5 in chrome can't into AAC so we use public3 which sounds close to public2
	 Safari however can so we will try using it on Apple devices running Safari
	*/
	var is_safari = (navigator.userAgent.indexOf("Safari") > -1) && (navigator.userAgent.indexOf("Chrome") == -1);
	window.DITier = 'public3';
	if(is_safari) window.DITier='public2'; // Hope this works? Safari users please test
	console.log('loading di tier '+window.DITier);
	$.ajax({url: 'http://listen.di.fm/'+window.DITier, success: function(data) {
		var resultantcode = '';
		var i=0;
		data.sort(DI_SortStations);
		for(i = 0; i<data.length-1;i++) {
			var stname = data[i].name;
			var stkey = data[i].key;
			var stdesc = data[i].description;
			var cur = DI_BuildCell(stkey, stname, stdesc, i+1, '');
			window.DIStationNames[stkey]=stname;
			window.DIStationIDs[stkey]=data[i].id;
			resultantcode += cur;
		}
		writeUI(resultantcode, uiname)
		NProgress.done();
	}});
}

function DI_BuildCell(key, name, description, iterator, accessory) {
	var identity = 'DI'+key;
	var cell='';
	cell = '<li class="song" id="" onclick="loadFile(\''+identity+'\')"><span class="song-number"><span class="format" id="'+identity+'_format">DI</span><img src="img/now.png" class="now" id="'+identity+'_ico"></i> '+iterator+'</span>'+accessory+'<span class="song-name" id="'+identity+'_title">'+name+'</span><span class="song-time">--:--</span><span class="song-artist" id="'+identity+'_author">'+description+'</span>';
			cell+= "</li>";
	return cell;
}

function DI_StartStation(stkey) {
	console.log('Starting DI station '+stkey);
	$.ajax({url: 'http://listen.di.fm/'+window.DITier+'/'+stkey, success: function(data) {
		data = data.filter(function(item){ return item.indexOf('di.fm') > -1; });
		var station = data[0]; //blindly assume it works best, add a try-each-one routine maybe?
		DI_URLReceived(station, stkey);
	}});
}

function DI_URLReceived(url, stkey) {
	window.audio = new Audio();
	window.audio.src =url; 
	window.audio.play();
			
	window.audio.addEventListener("stalled", function() {popMessage(window.locale.ui.bufferingStalled);});
			window.audio.addEventListener("canPlay", function()
				  {
					document.getElementById('btn-pause').style.display="block";
					document.getElementById('btn-play').style.display="none";
					window.audio.volume = $( ".volume-bar" ).volslider('option','value') / 100;
					window.audio.play();
				  }
				);
				window.audio.addEventListener("ended", function(e)
				  {
				  	stop();
				  }
				);
				document.getElementById('btn-pause').style.display="block";
				document.getElementById('btn-play').style.display="none";
				window.audio.volume = $( ".volume-bar" ).volslider('option','value') / 100;
				$('#displ')[0].style.opacity="1";
				$( ".track-progress" ).seekslider('option','disabled',true);
				$( ".track-progress" )[0].style.opacity = "0";
				$( ".track-position-time" )[0].style.display = "none";
				$( ".track-position-remaining" )[0].style.display = "none";
				$( ".volume-bar" ).volslider('option','disabled', false);
				disableProg();
				stationName  =window.DIStationNames[stkey];
				document.getElementById('song-title').innerHTML='Digitally Imported '+stationName;
				document.getElementById('song-album').innerHTML=stationName;
				document.getElementById('song-band').innerHTML="DI: "+stationName; 
				document.getElementById('sharer').style.display="none";
				document.getElementById('keeper').style.display="none";
				postMetadata(true, stationName, 'Radio', 'Radio');
				showAlbum();
				clearTimeout(displayBandTimeout);
				DI_SubscribeToMetaUpdates(stkey);
				for(var a=0; a < $('.now').length; a++) { $('.now')[a].style.display='none'; }
				for(var a=0; a < $('.format').length; a++) { $('.format')[a].style.display='inline-block'; }
				try {
					document.getElementById('DI'+stkey.toString()+'_format').style.display="none";
					document.getElementById('DI'+stkey.toString()+'_ico').style.display="inline-block";
				} catch(e) {}

}
var DICurMeta = new Object;

function DI_SubscribeToMetaUpdates(stkey) {
	
	$.ajax({url:'http://api.audioaddict.com/v1/di/track_history/channel/'+window.DIStationIDs[stkey].toString(), success:function(data){
				metaWatchTimer = setTimeout(function(){DI_SubscribeToMetaUpdates(stkey)}, 5000);
				var it = 0;
				while(typeof data[it].ad != 'undefined') {
					it = it +1;
				}
		if(DICurMeta == data[it].title) return;
		DICurMeta = data[it].title;
	showBand();
			document.getElementById('song-title').innerHTML=data[it].title;
				document.getElementById('song-album').innerHTML="DI: "+stationName;
				document.getElementById('song-band').innerHTML=data[it].artist; 
				document.getElementById('sharer').style.display="none";
				document.getElementById('keeper').style.display="none";
				postMetadata(!window.audio.paused, data[it].title, data[it].artist, 'Digitally Imported '+window.DIStationNames[stkey], data[it]['art_url']);
	

	}});	
}



function DI_SortStations(a,b) {
	var temparr = [a.name, b.name];
	temparr.sort();
	// This was built experimentally :p
	if(temparr[0] == a.name) {
		return -1;
	} else if (temparr[0] == b.name) {
		return 1;
	} else {
		return 0;
	}
}