
//@ http://jsfromhell.com/array/shuffle [v1.0]
function arrayShuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};


function loadFile(url, isNotManual) { 
	init_notify();
	hadstarted=false;
	if (typeof metaWatchTimer != 'undefined') {
		clearTimeout(metaWatchTimer);
	}
	curUrl = url;
	window.curFile=url;
	$('#displ')[0].style.opacity=1;
	$( ".track-progress" )[0].style.opacity = "1";
				$( ".track-position-time" )[0].style.display = "table-cell";
				$( ".track-position-remaining" )[0].style.display = "table-cell";
	if(window.player != undefined) {
		window.player.asset.stop();
		window.player.stop();
		window.player = undefined;
	}
	if(window.audio != undefined) {
		window.audio.pause();
		window.audio = undefined;
	}
	if(url.substr(0, 2)== 'VK') {	
		loadVK(url);
	} else if(url.substr(0, 2)== 'SC') {
		loadCloud(url, !isNotManual);
	} else if(url.substr(0, 2)== 'RA') {
		loadRadio(url.substr(2, url.length));
	} else if(url.substr(0, 2)== 'DI') {
		DI_StartStation(url.substr(2, url.length));
	} else {
		loadCogs(url);
	}
	if(isNotManual == true) return;
	buildQueue(url);
 }
function playPause() {
	init_notify();
	if(window.player == undefined && window.audio == undefined) return;
	if(window.player != undefined) {
		if(window.player.playing) {
			window.player.pause();
			document.getElementById('btn-pause').style.display="none";
			document.getElementById('btn-play').style.display="block";
			window.curPState.playing = false;
			unity.sendState(window.curPState);
		} else {
			window.player.play();
			document.getElementById('btn-pause').style.display="block";
			document.getElementById('btn-play').style.display="none";
			window.curPState.playing = true;
			unity.sendState(window.curPState);
		}
	}
	if(window.audio != undefined) {
		if(!window.audio.paused) {
			window.audio.pause();
			document.getElementById('btn-pause').style.display="none";
			document.getElementById('btn-play').style.display="block";
			window.curPState.playing = false;
			unity.sendState(window.curPState);
		} else {
			window.audio.play();
			document.getElementById('btn-pause').style.display="block";
			document.getElementById('btn-play').style.display="none";
			window.curPState.playing = true;
			unity.sendState(window.curPState);
		}
	}
}

var curUrl='';

window.playingFromUI='';
window.curFile='';
// ---- For SoundCloud Fallback Only ----
window.trackNames = new Object;
window.trackArtists = new Object;
// --------------------------------------
 window.queue=new Array;
 window.curQueueItem=-1;
 var updater=null;	var samplesFix = null; 

function buildQueue(url) {
	
	if(hasSwitched) {
		window.queue = new Array;
	}
		window.playingFromUI=window.curUI;

	window.curQueueItem=-1;
	var canAdd=true;
	var iterator=0;
		if (hasSwitched) {
			for(var a=0; a < $('.song').length; a++) { // calculate queue
					window.queue.push($('.song')[a].id);
					
					if(window.playingFromUI.substr(0, 2) == 'sc') {
						// If SoundCloud, make a dictionary of all users and tracks in playlist since there isnt a way to get metadata for it
						try {
							window.trackNames[$('.song')[a].id] = $('.song')[a].children[1].innerHTML;
							window.trackArtists[$('.song')[a].id] = $('.song')[a].children[3].innerHTML;
						} catch(e) {}
					}
			}
		}
			window.curQueueItem=window.queue.indexOf(url);
		hasSwitched=false;
		if(window.isShufflin) {
			shuffleQ();
		}
}

function playPause() {
	init_notify();
	if(window.player == undefined && window.audio == undefined) return;
	if(window.player != undefined) {
		if(window.player.playing) {
			window.player.pause();
			document.getElementById('btn-pause').style.display="none";
			document.getElementById('btn-play').style.display="block";
			window.curPState.playing = false;
			unity.sendState(window.curPState);
		} else {
			window.player.play();
			document.getElementById('btn-pause').style.display="block";
			document.getElementById('btn-play').style.display="none";
			window.curPState.playing = true;
			unity.sendState(window.curPState);
		}
	}
	if(window.audio != undefined) {
		if(!window.audio.paused) {
			window.audio.pause();
			document.getElementById('btn-pause').style.display="none";
			document.getElementById('btn-play').style.display="block";
			window.curPState.playing = false;
			unity.sendState(window.curPState);
		} else {
			window.audio.play();
			document.getElementById('btn-pause').style.display="block";
			document.getElementById('btn-play').style.display="none";
			window.curPState.playing = true;
			unity.sendState(window.curPState);
		}
	}
}



function stop() {
	if(window.player == undefined && window.audio == undefined) return;
	if(window.player != undefined) window.player.stop();
	if(window.audio != undefined) window.audio.pause(); 
	clearTimeout(displayBandTimeout);
	document.getElementById('btn-pause').style.display="none";
	document.getElementById('btn-play').style.display="block";
	$('#displ')[0].style.opacity="0";
	postMetadata(false, '', '', '');
	for(var a=0; a < $('.now').length; a++) { $('.now')[a].style.display='none'; }
}

function next() {
	if(window.player != undefined) if(!window.player.playing) return;
	if (window.queue == undefined || window.queue.length == 0) { alert(window.locale.errors.queueIsUndefined); return; }
	window.curQueueItem++;
	if(window.curQueueItem >= window.queue.length) {
		
		$('#displ')[0].style.opacity="0";
		stop();
		window.curQueueItem=0;
	
		return;
	}
	loadFile(window.queue[window.curQueueItem],true);
	
}
function prev() {

	if(window.player != undefined) if(!window.player.playing) return;
	if(window.audio != undefined) if(window.audio.paused) return;
	clearTimeout(samplesFix);
	if (window.queue == undefined || window.queue.length == 0) { alert(window.locale.errors.queueIsUndefined); return; }
	window.curQueueItem--;
	if(window.curQueueItem < 0) {
		$('#displ')[0].style.opacity="0";
		window.curQueueItem=window.queue.length-1;
		stop();
		
		return;
	}
	loadFile(window.queue[window.curQueueItem],true);
	
}

window.curPState = new Object();

function postMetadata(isPlaying, song, artist, album, art) {
	if(isPlaying) notify(song, artist, art);
	if(typeof window.unity == 'undefined') return;
	if(typeof art == 'undefined' || art == '') art = window.location.href.split('player.php')[0]+"/webtunes.png";
	window.curPState = {
	  playing: isPlaying,
	  title: song,
	  artist: artist,
	  album: album,
	  albumArt: art
	};
	unity.sendState(window.curPState);
}

function loadCogs(url) {
	//  go on with Audiocogs
		if(window.newplayer != undefined)
		{
			window.player = undefined;
			window.player = window.newplayer;	
		} else {
			window.player = AV.Player.fromURL(url);
		}
		// Preload file off url
		window.player.preload();
		window.player.asset.on('format', function() {
			window.player.asset.get('metadata', function(m) { // Got metadata, display it
				$('#displ')[0].style.opacity="1";
				var f = window.player.asset.source.url.split('.');
				var ext = f[f.length-1];
				if(m.title == undefined || m.title == '' || ext.toLowerCase() == 'wav') {
					var a = window.player.asset.source.url.split('/');
					document.getElementById('song-title').innerHTML=a[a.length-1];
					document.title = window.locale.appname+': '+a[a.length-1];
				} else{
					document.getElementById('song-title').innerHTML=m.title;
					document.title = window.locale.appname+': '+m.title+' - '+m.artist;
				}
				if(ext.toLowerCase() == 'flac') {
					$( ".track-progress" ).seekslider('option','disabled',true);
				} else {
					$( ".track-progress" ).seekslider('option','disabled',false);
				}
				
				document.getElementById('song-album').innerHTML=m.album;
				document.getElementById('song-band').innerHTML=m.artist;
				
				postMetadata(true, document.getElementById('song-title').innerHTML, m.artist, m.album);
				
				clearTimeout(displayBandTimeout);
				showBand();
		  	});
		});
		
		
		window.player.asset.on('buffer', function(p) { // Buffering progress
			document.getElementById('progressbar-footer').style.width=p.toString()+"%";
			if(p == 100) {
				document.getElementById('footer-process').style.display="none";
				
				$('body')[0].style.cursor = "default";
			} else {
				document.getElementById('progressbar-footer').style.width=p.toString()+"%";
				
				document.getElementById('footer-process').style.display="block";
				document.getElementById('process-sig').innerHTML=window.locale.ui.buffering;
				$('body')[0].style.cursor = "progress";
			}
			if(p >= 25 && !hadstarted) { 
			  
					window.player.play();
			
			 hadstarted=true;
				document.getElementById('btn-pause').style.display="block";
			document.getElementById('btn-play').style.display="none";
			 }
		});
		
		window.player.on('progress', function(pr) {
			updateProg(); // update progress
		});
		
		window.player.on('error', function(e) {
			popMessage(window.locale.errors.playError+': '+e);
			next();
		});
		
		window.player.volume = $( ".volume-bar" ).volslider('option','value'); // set volume
		performNowPlayingIconRescan(url);

		// show share and DL button
		document.getElementById('sharer').style.display="table-cell";
			document.getElementById('keeper').style.display="table-cell";
}

function seekHandler(ui) { // jquery ui handler
	if(window.player == undefined) return;
      	if(window.player.buffered < 100) {
	      	$( ".track-progress" ).seekslider('option','value',(window.player.currentTime / window.player.duration)*1000);
	      	return;
      	}
        window.player.seek(Math.round((window.player.duration / 1000) * ui.value));
}

// ----------------------------------
window.isShufflin = false;
window.preShuffleQ = new Array();
// ----------------------------------
function shuffleQ() {
	window.isShufflin = true;
		
		window.preShuffleQ = window.queue.slice(0);
		var nbackup = undefined;
		if(window.curQueueItem > -1) {
			nbackup = window.queue[window.curQueueItem];
			delete window.queue[window.curQueueItem];
		}
		window.queue = arrayShuffle(window.queue);
		window.queue.unshift(nbackup);
		window.queue =  window.queue.filter(function(n){return n});
		window.curQueueItem = 0;
}

function unshuffleQ() {
		window.queue =  window.preShuffleQ.slice(0);
		window.preShuffleQ = new Array();
		window.isShufflin = false;
		window.curQueueItem = window.queue.indexOf(curUrl);
}

function shuffle() {
	if(window.isShufflin) {
		unshuffleQ();
		$('#shu').removeClass('on');
	} else {
		shuffleQ();
		$('#shu').addClass('on');			
	}
}