//NOTLANG
// The above string designates the file is not a localization file

// Sekai.js -- a localization engine for the WebTunes project
// Soon to evolve in a separate plugin!

function prepareLocale() {
	window.locale = new Object();
	window.locale.ui = new Object();
	window.locale.dialogs = new Object();	
	window.locale.sidebar = new Object;
	window.locale.errors = new Object();
	window.locale.radio = new Object();
}

var isLoadLocaleInProgress = false;

function loadLocale(langcode) {
	if(isLoadLocaleInProgress) return;
	// Dynamically load a locale and then display it
	isLoadLocaleInProgress = true;
	makeStrings = undefined;
	if(window.locale == undefined)  prepareLocale();
	if(langcode == undefined || langcode == '') langcode = 'en';
	$.getScript("localization/"+langcode+'.js',  function(){
			makeStrings();
			localizeUI();		
			isLoadLocaleInProgress = false;
			window.language = langcode;
		});
} 

function localizeUI() {
	// iterate through <localized key=".." category=".."> elements
	// and set their inner html to window.locale.<category>.<key>
	
	var localPlaces = $('localized');
	var i = 0;
	for(i = 0; i < localPlaces.length; i++) {
		var cur = localPlaces[i];
		if(cur.getAttribute('category') == undefined) {
			cur.innerHTML = window.locale[cur.getAttribute('key')];
		} else {
			cur.innerHTML = window.locale[cur.getAttribute('category')][cur.getAttribute('key')];
		}
	}
	try {
		$('#scsearchfield')[0].placeholder = window.locale.ui.searchCloud;
	} catch(e) { }
	try {
		$('#vksearchfield')[0].placeholder = window.locale.ui.searchVK;
	} catch(e) { }
	document.title = window.locale.appname;
	locElementTitle ('loc_sharer', window.locale.ui.shareTune);
	locElementTitle ('loc_keeper', window.locale.ui.keepTune);
}


function locElement(element, text) {
	$('#'+element)[0].innerHTML=text;
}
function locElementByToken(element, token, text) {
	$('#'+element)[0].innerHTML = $('#'+element)[0].innerHTML.replace('__'+token+'__', text);
}
function locElementTitle (element, title) {
	$('#'+element)[0].title=title;
}
function localizeSidebar() {
	if(isLoadLocaleInProgress) return;
	localizeUI();
}
