<?
if(!isset($_SESSION)){ session_start(); }

require('../config.php');
include '../scapi/Soundcloud.php';

session_write_close();

$dlink = mysql_connect($dbhost,$dbuser,$dbpass, true, 0); // connect mysql
mysql_select_db($dbname, $dlink);
mysql_query ("set character_set_client='utf8'", $dlink); 
mysql_query ("set character_set_results='utf8'", $dlink); 
mysql_query ("set collation_connection='utf8_general_ci'", $dlink); 

$soundcloud = new Services_Soundcloud($sckey, $scsec, $webroot.'sc_auth.php'); //make new connection to SoundCloud

$uid=mysql_escape_string($_SESSION['uid']); // User id
$at = mysql_fetch_array(mysql_query("SELECT * FROM cloud WHERE uid='$uid'", $dlink)); // find if user was connected

$atok = unserialize($at['sc_token']); // if was connected, grab token

try { // if failing, falls back to connect to soundcloud button
  if(trim($_SESSION['lact']) != '') { // Had last action restored
    $action = $_SESSION['lact'];
    $_SESSION['lact'] = '';
  }
  if($atok != '') { // We had a token saved, load it
    $soundcloud->setAccessToken($atok);
  }
  if($action == '') { // No action specified, defaults to feed

  }
  
  if($action == 'own') {
    $tracks = json_decode($soundcloud->get('me/tracks'), true); // Load own tracks

         if(count($tracks) == 0) {
        die(json_encode(array('code'=>'ok', 'result'=>array())));
      }
      $ses = array();
      foreach($tracks as &$track) { // output each row
      
      $t = objectToArray($track);

        if($t['kind'] == 'track') {
          $stream_id = $t['id'];
          $title = $t['title']; $time = msToTime($t['duration']);
          $author = $t['user']['username'];
          $trk = array(
          	'fname'=>'SC'.$stream_id,
          	'title'=>$title,
          	'artist'=>$author,
          	'album'=>'',
          	'time'=>$time
          );
          	array_push($ses, $trk);
        }

      }
      die(json_encode(array('code'=>'ok', 'result'=>$ses)));
      
    }


  if($action == 'faves') {
    // list favorites
    $tracks = json_decode($soundcloud->get('me/favorites', array('limit' => 1000000)), true); // limit thing due to a bug
          if(count($tracks) == 0) {
        die(json_encode(array('code'=>'ok', 'result'=>array())));
      }
      $ses = array();
      foreach($tracks as &$track) { // output each row
      
      $t = objectToArray($track);

      
          $stream_id = $t['id'];
          $title = $t['title']; $time = msToTime($t['duration']);
          $author = $t['user']['username'];
          $trk = array(
          	'fname'=>'SC'.$stream_id,
          	'title'=>$title,
          	'artist'=>$author,
          	'album'=>'',
          	'time'=>$time
          );
          	array_push($ses, $trk);
        }

      die(json_encode(array('code'=>'ok', 'result'=>$ses)));     

  }
  
  
  if($action == 'playlist') {
    // list a list
    $par = $_GET['lid'];
    $list = objectToArray(json_decode($soundcloud->get('playlists/'.$par, array('limit' => 1000000)), true)); // limit thing due to a bug
    $iterator=0;
    $tracks = $list['tracks'];
    //var_dump($tracks); die();
    if(count($tracks) == 0) {
        die(json_encode(array('code'=>'ok', 'result'=>array())));
      }
      $ses = array();
      foreach($tracks as &$track) { // output each row
      
      $t = objectToArray($track);

          $stream_id = $t['id'];
          $title = $t['title']; $time = msToTime($t['duration']);
          $author = $t['user']['username'];
          $trk = array(
          	'fname'=>'SC'.$stream_id,
          	'title'=>$title,
          	'artist'=>$author,
          	'album'=>'',
          	'time'=>$time
          );
          	array_push($ses, $trk);

      }
      die(json_encode(array('code'=>'ok', 'result'=>$ses)));

  }

  if($action == 'search') {
    $par = $_GET['query'];

    if(trim($par) != '') {
      $tracks = json_decode($soundcloud->get('tracks', array('q' => $par, 'limit'=>1000000)));
      if(count($tracks) == 0) {
        die(json_encode(array('code'=>'ok', 'result'=>array())));
      }
      $ses = array();
      foreach($tracks as &$track) { // output each row
      
      $t = objectToArray($track);

        if($t['kind'] == 'track') {
          $stream_id = $t['id'];
          $title = $t['title']; $time = msToTime($t['duration']);
          $author = $t['user']['username'];
          $trk = array(
          	'fname'=>'SC'.$stream_id,
          	'title'=>$title,
          	'artist'=>$author,
          	'album'=>'',
          	'time'=>$time
          );
          	array_push($ses, $trk);
        }

      }
      die(json_encode(array('code'=>'ok', 'result'=>$ses)));

    }
  }

} catch (Services_Soundcloud_Invalid_Http_Response_Code_Exception $e) { // Error -- show connect button
  die(json_encode(array('code'=>'failure', 'detail'=>'notAvailable') ));

}



?>